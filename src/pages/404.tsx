import type { NextPage } from "next";

const Custom404: NextPage = () => {
  return (
    <div>
      <h1>404 - page not found!</h1>
    </div>
  );
};

export default Custom404;
