import Link from "next/link";
import Layout from "../components/Layout";

const Index = () => (
  <Layout title="Home | Next.js + TypeScript Example">
    <p>
      <Link href="/about">
        <a>About</a>
      </Link>
    </p>
  </Layout>
);

export default Index;
