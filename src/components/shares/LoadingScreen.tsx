import { useRouter } from "next/router";
import { useEffect } from "react";
import { Spinner } from "@vechaiui/react";

import layout from "styles/layout.module.css";
import gaEvent from "utils/analytics/gaEvent";

export default function CenteredCicularProgress() {
  const router = useRouter();
  useEffect(() => {
    const start = new Date();

    return () => {
      const end = new Date();
      const time = end.getTime() - start.getTime();
      const seconds = Math.round((time * 10) / 1000) / 10;

      gaEvent({
        category: "loading",
        action: "centered loader" + router.asPath,
        label: seconds + "s",
        nonInteraction: true,
      });
    };
  }, [router]);

  return (
    <div className={layout.center}>
      <Spinner size="xl" className="text-primary-500" />
    </div>
  );
}
